<?php
namespace Tunz\ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Tunz\ApiBundle\Entity\Song;
use Tunz\ApiBundle\Entity\Track;
use Tunz\ApiBundle\Entity\Clip;
use Tunz\ApiBundle\Entity\TrackSequence;

class LoadSongsData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $song = new Song();
        $song->setName('Test Song');
        $song->setTempo(120);
        $song->setDescription('A song description'); 
        $song->setBeatsPerMeasure(4);
        $song->setBeatUnit(4);
        $song->setLengthInMeasures(40);
        $song->setOwner($this->getReference('main-user'));
        $manager->persist($song);
        
        
        $clip = new Clip();
        $clip->setName('Test Clip');
        $clip->setInstrument('Guitar');
        $clip->setFilename('test-clip-guitar-01.wav');
        $clip->setSong($song);
        $clip->setLengthInMeasures(4);
        $manager->persist($clip);
        
        $trackSequence = new TrackSequence();
        $trackSequence->setMarkers(array(1, 6));
        $manager->persist($trackSequence);
        
        $track = new Track();
        $track->setName('Track 1');
        $track->setPriority(1);
        $track->setVolume(80);
        $track->setSong($song);
        $track->setClip($clip);
        $track->setSequence($trackSequence);
        $manager->persist($track);
        
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}