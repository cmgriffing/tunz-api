<?php
namespace Tunz\ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Tunz\ApiBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('testuser');
        $user->setPlainPassword('temppass');
        $user->setEmail('temp@localhost');
        $user->setEnabled(true);
        $manager->persist($user);
                
        $manager->flush();
        
        $this->addReference('main-user', $user);
    }
    
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}