<?php

namespace Tunz\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Song
 */
class Song
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $tempo;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Song
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tempo
     *
     * @param integer $tempo
     * @return Song
     */
    public function setTempo($tempo)
    {
        $this->tempo = $tempo;

        return $this;
    }

    /**
     * Get tempo
     *
     * @return integer 
     */
    public function getTempo()
    {
        return $this->tempo;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Song
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clips;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tracks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clips = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tracks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add clips
     *
     * @param \Tunz\ApiBundle\Entity\Clip $clips
     * @return Song
     */
    public function addClip(\Tunz\ApiBundle\Entity\Clip $clips)
    {
        $this->clips[] = $clips;

        return $this;
    }

    /**
     * Remove clips
     *
     * @param \Tunz\ApiBundle\Entity\Clip $clips
     */
    public function removeClip(\Tunz\ApiBundle\Entity\Clip $clips)
    {
        $this->clips->removeElement($clips);
    }

    /**
     * Get clips
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClips()
    {
        return $this->clips;
    }

    /**
     * Add tracks
     *
     * @param \Tunz\ApiBundle\Entity\Track $tracks
     * @return Song
     */
    public function addTrack(\Tunz\ApiBundle\Entity\Track $tracks)
    {
        $this->tracks[] = $tracks;

        return $this;
    }

    /**
     * Remove tracks
     *
     * @param \Tunz\ApiBundle\Entity\Track $tracks
     */
    public function removeTrack(\Tunz\ApiBundle\Entity\Track $tracks)
    {
        $this->tracks->removeElement($tracks);
    }

    /**
     * Get tracks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTracks()
    {
        return $this->tracks;
    }
    /**
     * @var \Tunz\ApiBundle\Entity\User
     */
    private $owner;


    /**
     * Set owner
     *
     * @param \Tunz\ApiBundle\Entity\User $owner
     * @return Song
     */
    public function setOwner(\Tunz\ApiBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Tunz\ApiBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
    /**
     * @var integer
     */
    private $beatsPerMeasure;

    /**
     * @var integer
     */
    private $beatUnit;

    /**
     * @var integer
     */
    private $lengthInMeasures;


    /**
     * Set beatsPerMeasure
     *
     * @param integer $beatsPerMeasure
     * @return Song
     */
    public function setBeatsPerMeasure($beatsPerMeasure)
    {
        $this->beatsPerMeasure = $beatsPerMeasure;

        return $this;
    }

    /**
     * Get beatsPerMeasure
     *
     * @return integer 
     */
    public function getBeatsPerMeasure()
    {
        return $this->beatsPerMeasure;
    }

    /**
     * Set beatUnit
     *
     * @param integer $beatUnit
     * @return Song
     */
    public function setBeatUnit($beatUnit)
    {
        $this->beatUnit = $beatUnit;

        return $this;
    }

    /**
     * Get beatUnit
     *
     * @return integer 
     */
    public function getBeatUnit()
    {
        return $this->beatUnit;
    }

    /**
     * Set lengthInMeasures
     *
     * @param integer $lengthInMeasures
     * @return Song
     */
    public function setLengthInMeasures($lengthInMeasures)
    {
        $this->lengthInMeasures = $lengthInMeasures;

        return $this;
    }

    /**
     * Get lengthInMeasures
     *
     * @return integer 
     */
    public function getLengthInMeasures()
    {
        return $this->lengthInMeasures;
    }
}
