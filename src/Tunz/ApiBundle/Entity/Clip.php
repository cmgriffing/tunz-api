<?php

namespace Tunz\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clip
 */
class Clip
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $instrument;

    /**
     * @var string
     */
    private $filename;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Clip
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set instrument
     *
     * @param string $instrument
     * @return Clip
     */
    public function setInstrument($instrument)
    {
        $this->instrument = $instrument;

        return $this;
    }

    /**
     * Get instrument
     *
     * @return string 
     */
    public function getInstrument()
    {
        return $this->instrument;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Clip
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }
    /**
     * @var \Tunz\ApiBundle\Entity\Song
     */
    private $song;


    /**
     * Set song
     *
     * @param \Tunz\ApiBundle\Entity\Song $song
     * @return Clip
     */
    public function setSong(\Tunz\ApiBundle\Entity\Song $song = null)
    {
        $this->song = $song;

        return $this;
    }

    /**
     * Get song
     *
     * @return \Tunz\ApiBundle\Entity\Song 
     */
    public function getSong()
    {
        return $this->song;
    }
    /**
     * @var integer
     */
    private $lengthInMeasures;


    /**
     * Set lengthInMeasures
     *
     * @param integer $lengthInMeasures
     * @return Clip
     */
    public function setLengthInMeasures($lengthInMeasures)
    {
        $this->lengthInMeasures = $lengthInMeasures;

        return $this;
    }

    /**
     * Get lengthInMeasures
     *
     * @return integer 
     */
    public function getLengthInMeasures()
    {
        return $this->lengthInMeasures;
    }
}
