<?php

namespace Tunz\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Track
 */
class Track
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $priority;

    /**
     * @var integer
     */
    private $volume;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Track
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return Track
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set volume
     *
     * @param integer $volume
     * @return Track
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return integer 
     */
    public function getVolume()
    {
        return $this->volume;
    }
    /**
     * @var \Tunz\ApiBundle\Entity\Clip
     */
    private $clip;

    /**
     * @var \Tunz\ApiBundle\Entity\Song
     */
    private $song;


    /**
     * Set clip
     *
     * @param \Tunz\ApiBundle\Entity\Clip $clip
     * @return Track
     */
    public function setClip(\Tunz\ApiBundle\Entity\Clip $clip = null)
    {
        $this->clip = $clip;

        return $this;
    }

    /**
     * Get clip
     *
     * @return \Tunz\ApiBundle\Entity\Clip 
     */
    public function getClip()
    {
        return $this->clip;
    }

    /**
     * Set song
     *
     * @param \Tunz\ApiBundle\Entity\Song $song
     * @return Track
     */
    public function setSong(\Tunz\ApiBundle\Entity\Song $song = null)
    {
        $this->song = $song;

        return $this;
    }

    /**
     * Get song
     *
     * @return \Tunz\ApiBundle\Entity\Song 
     */
    public function getSong()
    {
        return $this->song;
    }
    /**
     * @var \Tunz\ApiBundle\Entity\TrackSequence
     */
    private $sequence;


    /**
     * Set sequence
     *
     * @param \Tunz\ApiBundle\Entity\TrackSequence $sequence
     * @return Track
     */
    public function setSequence(\Tunz\ApiBundle\Entity\TrackSequence $sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return \Tunz\ApiBundle\Entity\TrackSequence 
     */
    public function getSequence()
    {
        return $this->sequence;
    }
}
