<?php

namespace Tunz\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackSequence
 */
class TrackSequence
{
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var array
     */
    private $markers;


    /**
     * Set markers
     *
     * @param array $markers
     * @return TrackSequence
     */
    public function setMarkers($markers)
    {
        $this->markers = $markers;

        return $this;
    }

    /**
     * Get markers
     *
     * @return array 
     */
    public function getMarkers()
    {
        return $this->markers;
    }
}
