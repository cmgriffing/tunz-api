<?php

namespace Tunz\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
    
        return $this->render('TunzApiBundle:Default:index.html.twig');
    }
}
