<?php

namespace Tunz\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class UserSongsController extends FOSRestController
{
    
    public function getSongsAction($userId)
    {
        $songs = $this->getDoctrine()->getRepository('TunzApiBundle:Song')->findByOwner($userId);
        
        $view = $this->view(array('songs' => $songs));
        
        return $this->handleView($view);
    }
    
}
