<?php

namespace Tunz\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class SongsController extends FOSRestController
{
    public function getSongAction($id) {
        $song = $this->getDoctrine()->getRepository('TunzApiBundle:Song')->findOneById($id);
        $view = $this->view(array('song' => $song));
        
        return $this->handleView($view);
    }
    
    public function getSongsAction() {
        
        $songs = $this->getDoctrine()->getRepository('TunzApiBundle:Song')->findAll();
        $view = $this->view(array('songs' => $songs));
        
        return $this->handleView($view);
    }
    
}
