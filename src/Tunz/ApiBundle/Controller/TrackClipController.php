<?php

namespace Tunz\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class TrackClipController extends FOSRestController
{
    public function getTrackClipAction()
    {
        
        $songs = $this->getDoctrine()->getRepository('TunzApiBundle:Song')->findAll();
        
        $view = $this->view($songs);
        
        return $this->handleView($view);
    }
}
