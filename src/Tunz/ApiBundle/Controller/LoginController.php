<?php

namespace Tunz\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

class LoginController extends FOSRestController
{
    public function getLoginAction() {
        
        $view = $this->view(array('success' => true));
        
        return $this->handleView($view);
    }
    
}
