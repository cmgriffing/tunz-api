<?php

namespace Tunz\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

class UserController extends FOSRestController
{

    public function getUserAction($userId)
    {
        
        $user = $this->getDoctrine()->getRepository('TunzApiBundle:User')->findById($userId);
        $view = $this->view(array('user' => $user->email));
        
        return $this->handleView($view);
    }

    public function getUserSaltAction($username) {
        $user = $this->getDoctrine()->getRepository('TunzApiBundle:User')->findOneBy(array('username' => $username));
        
        if(!$user) {
            return new Response('{"error" : "User does not exist."}');
            //this should probably be a throw to let the error handler take over
        }
        
        return new Response('{"id" : '.$user->getId().', "username" : "'.$username.'", "salt" : "'.$user->getSalt().'"}');
    }
    
}
