<?php

namespace Tunz\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

use Tunz\ApiBundle\Entity\Song;
use Tunz\ApiBundle\Entity\Clip;
use Tunz\ApiBundle\Entity\Track;
use Tunz\ApiBundle\Entity\TrackSequence;

class TracksController extends FOSRestController
{

    public function getTrackAction($songId, $trackId)
    {
        $track = $this->getDoctrine()->getRepository('TunzApiBundle:Track')->findOneById($trackId);
        
        $view = $this->view($track);
        
        return $this->handleView($view);
    }
    
    public function getTracksAction($songId)
    {
        $tracks = $this->getDoctrine()->getRepository('TunzApiBundle:Track')->findBySong($songId);
        
        $view = $this->view($tracks);
        
        return $this->handleView($view);
    }
    
    public function postTracksAction($songId, Request $request) {
        
        $song = $this->getDoctrine()->getRepository('TunzApiBundle:Song')->findOneById($songId);
        
        if(!$song) {
            //throw not found exception
            throw new FileNotFoundException('Song not found');
        }
        
        if(!$this->userIsOwnerOfSong($song)) {
            //throw access denied exception
            throw new AccessDeniedException('Access Denied');
        }
        
        $sequence = new TrackSequence();
        $sequence->setMarkers(array());
        
        $clip = null;
        
        if($request->request->get('clipId')) {
            $clip = $this->getDoctrine()->getRepository('TunzApiBundle:Clip')->findOneById($request->request->get('clipId'));
        }
                
        $track = new Track();
        $track->setSong($song);
        $track->setSequence($sequence);
        $track->setName($request->request->get('name'));
        $track->setPriority($request->request->get('priority'));
        $track->setVolume(80);
        if($clip) { $track->setClip($clip); }
        
        $errors = $this->get('validator')->validate($track);
        
        if(count($errors) > 0) {
            $errorString = (string)$errors;
            $view = new View();
            $view->setStatusCode('400');
            $view->setData(array('errors' => $errors));
            
            return $this->handleView($view);
            
        } else {
            //save track
            $em = $this->getDoctrine()->getManager();
            $em->persist($sequence);         
            $em->persist($track);
            $em->flush();
            
            $view = new View();
            $view->setStatusCode('201');
            $view->setHeader('Location', 
                $this->generateUrl(
                    'get_song_track', array('songId' => $song->getId(), 'trackId' => $track->getId()),
                    true // absolute
                )
            );
            $view->setData($track);
            
            return $this->handleView($view);
            
        }
        
    }
    
    /////////////////////////////////////////////////////
    ////  Private helpers
    /////////////////////////////////////////////////////
    
    private function userIsOwnerOfSong($song) {
        if($song->getOwner()->getId() === $this->getUser()->getId()) {
            return true;
        } else {
            return false;
        }
    }
}
