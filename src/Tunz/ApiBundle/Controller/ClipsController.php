<?php

namespace Tunz\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\Validator\Exception\ValidatorException;

use Tunz\ApiBundle\Entity\Song;
use Tunz\ApiBundle\Entity\Clip;

class ClipsController extends FOSRestController
{

    public function getClipsAction($songId) {
    
        $song = $this->getDoctrine()->getRepository('TunzApiBundle:Song')->findOneById($songId);
        
        if(!$song) {
            //render error
            return false;
        }
        
        if(!$this->userIsOwnerOfSong($song)) {
            //render error
            return false;
        }
    
        $clips = $this->getDoctrine()->getRepository('TunzApiBundle:Clip')->findBySong($songId);
        
        $view = $this->view($clips);
        
        return $this->handleView($view);
    }
    
    public function getClipAction($songId, $clipId) {
    
        $song = $this->getDoctrine()->getRepository('TunzApiBundle:Song')->findOneById($songId);
        
        if(!$song) {
            //render error
            return false;
        }
        
        if(!$this->userIsOwnerOfSong($song)) {
            //render error
            return false;
        }
        
        $clip = $this->getDoctrine()->getRepository('TunzApiBundle:Clip')->findOneById($clipId);
        
        if($clip->getSong()->getId() !== $songId) {
            //echo "Clip does not belong to song";
            //render error
        }
        
        $view = $this->view($clip);
        
        return $this->handleView($view);
    }
    
    public function postClipsAction($songId, Request $request) {
        
        $song = $this->getDoctrine()->getRepository('TunzApiBundle:song')->findOneById($songId);
        
        if(!$song) {
            //throw not found exception
            throw new FileNotFoundException('Song not found');
        }
        
        if(!$this->userIsOwnerOfSong($song)) {
            //throw access denied exception
            throw new AccessDeniedException('Access Denied');
        }
        
        $clip = new Clip();
        $clip->setSong($song);
        $clip->setName($request->request->get('name'));
        $clip->setInstrument($request->request->get('instrument'));
        $clip->setLengthInMeasures($request->request->get('lengthInMeasures'));
        $clip->setFilename($request->request->get('filename'));
        
        $errors = $this->get('validator')->validate($clip);
        
        if(count($errors) > 0) {
            $errorString = (string)$errors;
            $view = new View();
            $view->setStatusCode('400');
            $view->setData(array('errors' => $errors));
            
            return $this->handleView($view);
            
        } else {
            //save clip
            $em = $this->getDoctrine()->getManager();
            $em->persist($clip);
            $em->flush();
            
            $view = new View();
            $view->setStatusCode('201');
            $view->setHeader('Location', 
                $this->generateUrl(
                    'get_song_clip', array('songId' => $song->getId(), 'clipId' => $clip->getId()),
                    true // absolute
                )
            );
            $view->setData($clip);
            
            return $this->handleView($view);
            
        }
        
    }
    
    public function deleteClipAction($songId, $clipId, Request $request) {
    
        $song = $this->getDoctrine()->getRepository('TunzApiBundle:Song')->findOneById($songId);
        
        if(!$song) {
            //throw not found exception
            throw new FileNotFoundException('Song not found');
        }
        
        if(!$this->userIsOwnerOfSong($song)) {
            //throw access denied exception
            throw new AccessDeniedException('Access Denied');
        }
        
        $clip = $this->getDoctrine()->getRepository('TunzApiBundle:Clip')->findOneById($clipId);
        
        if(!$clip) {
            //throw not found exception
            throw new FileNotFoundException('Song not found');
        }
        
        if($clip->getSong()->getId() !== $song->getId()) {
            //throw access denied exception
            throw new AccessDeniedException('Access Denied');
        }
        
        $tracks = $this->getDoctrine()->getRepository('TunzApiBundle:Track')->findByClip($clip);
        
        if($tracks) {
            //Tracks exist so the clip can't be deleted
            throw new ValidatorException('There are tracks associated with this clip.');            
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($clip);
            $em->flush();
            
            $clips = $this->getDoctrine()->getRepository('TunzApiBundle:Clip')->findBySong($song);
            
            $view = new View();
            $view->setStatusCode('201');
            $view->setData($clips);
            
            return $this->handleView($view);
        }
        
    }
    
    
    
    /////////////////////////////////////////////////////
    ////  Private helpers
    /////////////////////////////////////////////////////
    
    private function userIsOwnerOfSong($song) {
        if($song->getOwner()->getId() === $this->getUser()->getId()) {
            return true;
        } else {
            return false;
        }
    }

}
